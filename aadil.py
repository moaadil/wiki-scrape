import os
import sys
import requests
import time
import re
import bs4 as bs
from builtins import input

def get_text(soup,pr,po):   #scraping the paras and saving into.txt file
    with open("{}/{}/{}_text.txt".format(pr,po,po), "w" ) as handler:
        for x in soup.find_all('p'):
            handler.write(x.get_text()+"\n")

def main():
    while True:
        url = input("\nEnter keyword to scrape > ")  #for eg: football
       
        url = "https://en.wikipedia.org/wiki/"+url

        try:
            web = requests.get(url).content
            break
        except:
            os.system('cls' if os.name in "nt" else "clear")  #to clear terminal when invalid
            continue

    soup = bs.BeautifulSoup(web,"lxml")
    prefix = re.findall(r"^https?://([^/]*)",url)[0]
    postfix = re.findall(r"^https?://[^/].*(/.*)",url)
    if postfix == []:
        postfix = "main"   #default keyword
    elif postfix[0] == "/":
        postfix = "main"
    else:
        postfix = postfix[0]

    try:
        os.makedirs("{}/{}".format(prefix,postfix))  #for recursive directory creation
    except:
        pass

    time.sleep(0.8)
    print("\nPress Enter to start scraping !\n")
    
    x = input("> ")
  
    if x == "":
        get_text(soup,prefix,postfix)
        print("\nDone\nContents were saved to {}/{}/\n".format(prefix,postfix))

        time.sleep(1)  

if __name__ == '__main__':
    main()
